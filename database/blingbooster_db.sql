-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2017 at 09:37 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blingbooster_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `item_id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `category` varchar(50) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `image` varchar(500) NOT NULL,
  `trade_status` varchar(10) NOT NULL DEFAULT 'INCOMPLETE',
  `user_giver_id_frkey` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `name`, `category`, `description`, `image`, `trade_status`, `user_giver_id_frkey`) VALUES
(1, 'Royce Wraith Palm - Edition 999', 'super cars', 'A detailed description of this item will be displayed in here.', 'car_royce-wraith-palm-edition-999.jpg', 'INCOMPLETE', 0),
(2, 'Ferrari 458 Speciale', 'super cars', 'A detailed description of this item will be displayed in here.', 'car_ferrari-458-speciale.JPG', 'INCOMPLETE', 0),
(3, 'A Private Luxury Villa on the Beach in Mauritius', 'holiday homes', 'A detailed description of this item will be displayed in here.', 'villa_cap_cap_malheureux_pool.jpg', 'INCOMPLETE', 0),
(4, 'Double Down Yacht', 'yachts', 'A detailed description of this item will be displayed in here.', 'yacht_double-down.jpg', 'INCOMPLETE', 0),
(5, 'Silver Dream Yacht', 'yachts', 'A detailed description of this item will be displayed in here.', 'yacht_silver-dream.jpg', 'INCOMPLETE', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trades`
--

CREATE TABLE IF NOT EXISTS `trades` (
  `trade_id` int(20) NOT NULL AUTO_INCREMENT,
  `percentage_taken` decimal(6,0) NOT NULL,
  `item_id_frkey` int(20) NOT NULL,
  `user_taking_id_frkey` int(20) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`trade_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `first_name`, `last_name`, `email`, `phone`, `password`) VALUES
(1, 'hassan.elmi', 'Hassan', 'Elmi', 'hassan.elmi@blingbooster.com', '0000', 'qwerty'),
(2, 'mohammad.nassar', 'Mohammad', 'Nassar', 'mohammad.nassar@blingbooster.com', '0000', '0000'),
(3, 'john.philip', 'John', 'Philip', 'john.philip@yahoo.com', '12002100', '0011'),
(4, 'sam.morris', 'Sam', 'Morris', 'sam.morris@gmail.com', '56781234', '5678'),
(5, '', 'Michael', 'Steve', 'michael.steve@yahoo.co.uk', '321321321', '4321'),
(6, 'ali.mustafa', 'Ali', 'Mustafa', 'ali.mustafa@yahoo.com', '43214321', '4321');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
